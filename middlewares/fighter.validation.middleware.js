const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    const fighterReq = req.body;
    const { name, power, defense, health = 100 } = fighterReq;
    const fightersEntitiesArr = Object.keys(fighterReq)


    if (fightersEntitiesArr.length < 3) {
        return res.status(404).json({
            status: 'error',
            code: 404,
            data: {
                message: 'Invalid credential.'
            }
        })
    }

    if (!fightersEntitiesArr.includes('name') || name === '') {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'Name is required'
            }
        })
    }

    if (!fightersEntitiesArr.includes('power') || power === '' || !fighter.power(power)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'Power is incorrect (number - min: 0, max: 100'
            }
        })
    }

    if (!fightersEntitiesArr.includes('defense') || defense === '' || !fighter.defense(defense)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'Defense is incorrect (number - min: 0, max: 10)'
            }
        })
    }

    // if (!fightersEntitiesArr.includes('health')) {
    //     return res.status(400).json({
    //         status: 'error',
    //         code: 400,
    //         data: {
    //             message: 'Health is required'
    //         }
    //     })
    // }


    next();
}

const updateFighterValid = (req, res, next) => {


    const fighterReq = req.body;
    const { name, power, defense } = fighterReq;
    const fightersEntitiesArr = Object.keys(fighterReq)


    if (fightersEntitiesArr.includes('name')) {

        if (name === '') {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'Name is incorrect'
                }
            })
        }

    }

    if (fightersEntitiesArr.includes('power')) {

        if (!fighter.power(power)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'Power is incorrect (number - min: 0, max: 100',
                }
            })
        }

    }

    if (fightersEntitiesArr.includes('defense')) {

        if (!fighter.defense(defense)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'Defense is incorrect (number - min: 0, max: 10)',
                }
            })
        }

    }


    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;