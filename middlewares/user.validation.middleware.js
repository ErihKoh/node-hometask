const { user } = require('../models/user');

const createUserValid = (req, res, next) => {


    const userReq = req.body;
    const { email, firstName, lastName, phoneNumber, password } = userReq;
    const userEntitiesArr = Object.keys(userReq)


    if (userEntitiesArr.length < 5) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'Invalid credential.'
            }
        })
    }

    if (!userEntitiesArr.includes('email') || email === '' || !user.email(email)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'Email is incorrect (***@gmail.com)'
            }
        })
    }


    if (!userEntitiesArr.includes('firstName') || !user.firstName(firstName)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'FirstName is required'
            }
        })
    }

    if (!userEntitiesArr.includes('lastName') || !user.lastName(lastName)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'LastName is required'
            }
        })
    }
    if (!userEntitiesArr.includes('phoneNumber') || phoneNumber === '' || !user.phoneNumber(phoneNumber)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'PhoneNumber is incorrect (+3801234567)'
            }
        })
    }

    if (!userEntitiesArr.includes('password') || password === '' || !user.password(password)) {
        return res.status(400).json({
            status: 'error',
            code: 400,
            data: {
                message: 'Password is incorrect (min 3 symbols)'
            }
        })
    }


    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    const userReq = req.body;
    const { email, firstName, lastName, phoneNumber, password } = userReq;
    const userEntitiesArr = Object.keys(userReq)

    // if (userEntitiesArr.includes('email') || userEntitiesArr.includes('firstName') || userEntitiesArr.includes('lastName') || userEntitiesArr.includes('phoneNumber') || userEntitiesArr.includes('password')) {
    //     return res.status(400).json({
    //         status: 'error',
    //         code: 400,
    //         data: {
    //             message: 'Invalid credential.'
    //         }
    //     })
    // }

    if (userEntitiesArr.includes('email')) {

        if (!user.email(email)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'Email is incorrect (***@gmail.com)'
                }
            })
        }

    }


    if (userEntitiesArr.includes('firstName')) {
        if (!user.firstName(firstName)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'FirstName is required'
                }
            })
        }

    }

    if (userEntitiesArr.includes('lastName')) {
        if (!user.lastName(lastName)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'LastName is required'
                }
            })
        }

    }
    if (userEntitiesArr.includes('phoneNumber')) {
        if (!user.phoneNumber(phoneNumber)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'PhoneNumber is incorrect (+3801234567)'
                }
            })
        }

    }

    if (userEntitiesArr.includes('password')) {
        if (!user.password(password)) {
            return res.status(400).json({
                status: 'error',
                code: 400,
                data: {
                    message: 'Password is incorrect (min 3 symbols)'
                }
            })
        }

    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;